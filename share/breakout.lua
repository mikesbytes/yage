win = ygp.Window.new()
handler = ygp.InputHandler.new(win)

running = true
function quitf ()
   running = false
end
del = handler:getEventDelegate(SDL.QUIT)
del:attach(quitf)
win:create()

cam = ygp.Camera.new(0,0,win.size.x,win.size.y)

-- paddle
paddlewidth = 100
paddlex = (win.size.x / 2) - (paddlewidth / 2)
paddle = ygp.Quad.new(paddlex, 25, paddlewidth, 25)

-- ball
ballsize = 10
ball = ygp.Quad.new(0,0,10,10);
ballVel = glm.vec2.new(0, 200)
function resetBall()
   pPos = paddle.position
   ball.position = glm.vec2.new(pPos.x + (paddlewidth / 2) - (ballsize / 2), 50)
end
resetBall()

local board = {}
local boardwidth = win.size.x - 100
local blockwidth = boardwidth / 20
for i = 0, 19, 1 do
   for j = 0, 9, 1 do
	  table.insert(board, ygp.Quad.new(50 + (i * blockwidth), (win.size.y - 70 - (j * 20)), blockwidth, 20))
   end
end

-- variables
active = false
clock = ybs.Clock.new()

while(running) do
   delta = clock:restart()
   handler:process()

   if handler:isKeyPressed("A") then
	  paddlex = paddlex - (200 * delta)
   end
   if handler:isKeyPressed("D") then
	  paddlex = paddlex + (200 * delta)
   end
   if handler:isKeyPressed("Space") and not active then
	  active = true
	  --set ball angle
	  diff = (ball.position.x + (ballsize / 2)) - (paddlex + (paddlewidth / 2))
	  diff = diff / (paddlewidth / 2)
	  ballVel.x = diff * 200
   end


   local bPos = ball.position
   if active then
	  -- move ball
	  ball.position = (bPos + ballVel * delta)
   end

   --bounds checking on ball
   -- top wall
   if bPos.y + ballsize > win.size.y then
	  ballVel = glm.vec2.new(ballVel.x, -ballVel.y)
	  ball.position = glm.vec2.new(bPos.x, win.size.y - ballsize)
   end

   -- paddle
   if bPos.x + ballsize > paddlex and bPos.x < paddlex + paddlewidth and bPos.y < 50 then
	  diff = (ball.position.x + (ballsize / 2)) - (paddlex + (paddlewidth / 2))
	  diff = diff / (paddlewidth / 2)
	  ballVel = glm.vec2.new(ballVel.x + diff * 200, -ballVel.y):normalize() * 200
	  ball.position = glm.vec2.new(bPos.x, 50)
   end

   -- side wall
   if bPos.x < 0 then
	  ballVel = glm.vec2.new(-ballVel.x, ballVel.y)
	  ball.position = glm.vec2.new(0, bPos.y)
   end
   if bPos.x + ballsize > win.size.x then
	  ballVel = glm.vec2.new(-ballVel.x, ballVel.y)
	  ball.position = glm.vec2.new(win.size.x - ballsize, bPos.y)
   end
   
   -- board
   local toremove = nil
   for k,block in pairs(board) do
	  col = block:aabb():collide(ball:aabb())
	  if (col:colliding()) then
		 ball.position = bPos + col:colVec()
		 ballVel = ballVel:reflect(col:colVec():normalize())
		 toremove = k
	  end
   end
   if toremove then
	  table.remove(board, toremove)
   end
   
   paddle.position = glm.vec2.new(paddlex, 25)
   
   win:clear()
   paddle:draw(cam)
   ball:draw(cam)
   for _,block in pairs(board) do
	  block:draw(cam)
   end
   win:swap()
end
