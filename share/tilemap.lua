local win = ygp.Window.new()
local handler = ygp.InputHandler.new(win)
local scale = 32

local running = true
handler:getEventDelegate(SDL.QUIT):attach(function ()
	  running = false
										 end)
win:create()
local cam = ygp.Camera.new(0,0, win.size.x / scale, win.size.y / scale)

-- load textures
local atManager = ygp.ArrayTextureManager.new()
atManager:newResource("base_tile_set", 32, 32)
local tex = atManager:getResource("base_tile_set")

tex:setTextureAt(0, ygp.Bitmap.new("share/exclamation.png"))
tex:setTextureAt(1, ygp.Bitmap.new("share/brick.png"))

-- load tilemap
local tiles = yph.Tiles.shared(32,32)
local tm = ygp.TileMap.new(1, 0, 0, 32, 32, tex, tiles)

local level = {
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
   0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,0,0,0,0,0,
   0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,0,0,0,0,
   0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,0,0,0,2,2,2,2,0,0,0,
   2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,0,2,2,2,2,2,2,2,
   2,2,0,2,2,2,2,2,2,2,0,0,0,2,2,2,2,2,2,2,2,2,0,0,0,2,2,2,2,2,2,2
}

for i, val in pairs(level) do
   local x = math.floor((i - 1) % 32)
   local y = math.floor(32 - ((i ) / 32))
   tiles:set(x,y,val)
end
tm:rebuildMesh()

local tCollider = tm:collider()

--player
local startPos = glm.vec2.new(0,2)
local pWidth = 1
local pHeight = 1.75 --24px high
local cornerCut = 0.2
local groundAccel = 30
local groundDecel = 55
local airAccel = 15
local airDecel = 25
local groundTopSpeed = 8
local jumpVel = 18.5
local bottomTouch = false

local player = ygp.Quad.new(-cornerCut,0,pWidth,pHeight)
local vCollider = yph.AABBCollider.new(startPos.x + cornerCut, startPos.y, pWidth - (cornerCut * 2), pHeight)
local hCollider = yph.AABBCollider.new(0,0,pWidth, pHeight - (cornerCut * 2))

local pWorld = yph.PhysicsWorld.new()

tCollider = pWorld:addCollider(tCollider)
vCollider = pWorld:addCollider(vCollider)
hCollider = pWorld:addCollider(hCollider)

player:setFollow(vCollider)


local pVel = glm.vec2.new(0,0)

local clock = ybs.Clock.new()

local jumps = 0
local jumping = false

vCollider:touched():attach(function (col)
	  vCollider.position = vCollider.position + glm.vec2.new(0, col.colVec.y)
	  hCollider.position = vCollider.position + glm.vec2.new(-cornerCut, cornerCut)
	  if col.colVec.y > 0 and pVel.y < 0 then
		 pVel.y = 0
		 jumps = 0
		 bottomTouch = true
	  end
	  if col.colVec.y < 0 and pVel.y > 0 then
		 pVel.y = 0
	  end
						  end)

hCollider:touched():attach(function (col)
	  hCollider.position = hCollider.position + glm.vec2.new(col.colVec.x, 0)
	  vCollider.position = hCollider.position + glm.vec2.new(cornerCut, -cornerCut)
	  pVel.x = 0
						  end)

while(running) do
   delta = clock:restart()
   handler:process()

   -- move camera
   if handler:isKeyPressed("D") then
	  if pVel.x > 0  then
		 if pVel.x < groundTopSpeed then
			pVel.x = pVel.x + (delta * groundAccel)
		 end
	  else
		 pVel.x = pVel.x + (delta * groundDecel)
	  end
   end

   local curAccel = groundAccel
   local curDecel = groundDecel
   if not bottomTouch then -- player is in the air
	  curAccel = airAccel
	  curDecel = airDecel
   end

   if handler:isKeyPressed("A") then
	  if pVel.x < 0 then
		 if pVel.x > -groundTopSpeed then
			pVel.x = pVel.x - (delta * curAccel)
		 end
	  else
		 pVel.x = pVel.x - (delta * airDecel)
	  end
   end
   if not handler:isKeyPressed("A") and not handler:isKeyPressed("D") then
	  if pVel.x > 0 then
		 pVel.x = math.max(0, pVel.x - (delta * curDecel))
	  else
		 pVel.x = math.min(0, pVel.x + (delta * curDecel))
	  end
   end
   
	  
   if handler:isKeyPressed("Space") then
	  if not jumping and jumps == 0 then
	     jumping = true
	     pVel.y = jumpVel
	     jumps = 1
	  end
   else
	  jumping = false
   end

   pVel.y = pVel.y - delta * 40
   vCollider.position = vCollider.position + (pVel * delta)
   hCollider.position = vCollider.position + glm.vec2.new(-cornerCut, cornerCut)

   bottomTouch = false
   pWorld:update()

   cam.position = vCollider.position - glm.vec2.new(10,10)

   win:clear()
   tm:draw(cam)
   player:draw(cam)
   win:swap()
end

