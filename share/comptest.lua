print("test")
local win = ygp.Window.new()
win:create()

local reg = entt.registry.new()
local handler = ygp.InputHandler.new(win)

local running = true
handler:getEventDelegate(SDL.QUIT):attach(function ()
	running = false
end)


local world = yph.PhysicsWorld.new()
local box = ygp.Quad.new(0,0,50,50)
local cam = ygp.Camera.new(0,0, win.size.x, win.size.y)

local ent = reg:create()

local pos = reg:assignPosition(ent)
pos.x = 5
pos.y = 10

local vel = reg:assignVelocity(ent)
vel.dX = 1
vel.dY = 2

local clock = ybs.Clock.new()
while(running) do
   local delta = clock:restart()
   handler:process()

   world:update(delta, reg)
   box.position = glm.vec2.new(pos.x, pos.y)

   win:clear()
   box:draw(cam)
   win:swap()
end
