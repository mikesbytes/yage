#version 330 core
out vec4 outColor;

in vec3 texCoord;

uniform sampler2DArray texSampler;

void main() {
	 outColor = texture(texSampler, texCoord);
}