Vector = {}
Vector.__index = Vector

function Vector.new(x,y)
   return setmetatable({ x = x or 0, y = y or 0}, Vector)
end


function Vector:__add(rhs)
   if type(rhs) == "number" then
	  return Vector.new(self.x + rhs, self.y + rhs)
   else
	  return Vector.new(self.x + rhs.x, self.y + rhs.y)
   end
end

function Vector:__sub(rhs)
   if type(rhs) == "number" then
	  return Vector.new(self.x - rhs, self.y - rhs)
   else
	  return Vector.new(self.x - rhs.x, self.y - rhs.y)
   end
end

function Vector:__mul(rhs)
   if type(rhs) == "number" then
	  return Vector.new(self.x * rhs, self.y * rhs)
   else
	  return Vector.new(self.x * rhs.x, self.y * rhs.y)
   end
end
   
function Vector:__div(rhs)
   if type(rhs) == "number" then
	  return Vector.new(self.x / rhs, self.y / rhs)
   else
	  return Vector.new(self.x / rhs.x, self.y / rhs.y)
   end
end

-- convert vector to glm.vec2
function Vector:glm()
   return glm.vec2.new(self.x, self.y)
end

function Vector:len()
   return math.sqrt(self.x^2 + self.y^2)
end

function Vector:dot(rhs)
   return self.x * rhs.x + self.y * rhs.y
end

function Vector:normalize()
   local len = self.len()
   self.x = self.x / len
   self.y = self.y / len
end

function Vector:normalized()
   return self / self:len()
end

