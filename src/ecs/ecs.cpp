#include "ecs/ecs.hpp"

namespace yecs {

void ECS::update(const double &delta) {
	for (auto&& i : mSystems) {
		i->update(delta);
	}
}

}
