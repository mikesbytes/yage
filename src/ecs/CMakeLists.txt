add_library(yageecs
			ecs.cpp
			system.cpp
			scriptinterface.cpp)

find_package(SDL2 REQUIRED)

set_target_properties(yageecs PROPERTIES
					  CXX_STANDARD 17)

target_link_libraries(yageecs sol2::sol2_single)
target_include_directories(yageecs PUBLIC
										../../include
										/usr/include/luajit-2.0
										../../extern/sol2/single/include/sol
							../../extern/entt/src/entt)
