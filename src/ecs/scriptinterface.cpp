#include "ecs/scriptinterface.hpp"

#include <sol.hpp>
#include <entt.hpp>

namespace yecs {

int registerScriptInterface(::sol::state& lua) {
	sol::table enttTab = lua.create_named_table("entt");
	auto meta = enttTab.new_usertype<entt::registry<>>("registry");

	meta["create"] = [](entt::registry<>& reg){return reg.create();};

	return 0;
}

}
