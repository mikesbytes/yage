#include "base/clock.hpp"

namespace ybs {

Clock::Clock() :
	mLastStart(std::chrono::steady_clock::now())
{
}

double Clock::restart() {
	auto curTime = std::chrono::steady_clock::now();
	auto timeSpan = std::chrono::duration_cast<std::chrono::duration<double>>(curTime - mLastStart);
	mLastStart = curTime;
	return timeSpan.count();
}

}
