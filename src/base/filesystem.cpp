#include "base/filesystem.hpp"

#include <fstream>
#include <algorithm>

namespace ybs {

FileSystem::FileSystem() {
	
}

std::vector<uint8_t> FileSystem::getFileAsBytes(const std::string &handle) {
	std::basic_ifstream<char> stream(handle, std::ios::in | std::ios::binary);
	std::vector<uint8_t> buffer;
	std::for_each(std::istreambuf_iterator<char>(stream),
	              std::istreambuf_iterator<char>(),
	              [&buffer](const char c) {
		              buffer.push_back(c);
	              });
	return buffer;
}

std::string FileSystem::getFileAsString(const std::string& handle) {
	auto data = getFileAsBytes(handle);
	return std::string(data.begin(), data.end());
}

}
