#include "base/scriptinterface.hpp"
#include "base/luadelegate.hpp"
#include "base/clock.hpp"
#include "sol.hpp"

#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>

namespace ybs {

int registerScriptInterface(::sol::state& lua) {
	sol::table glmTab = lua.create_named_table("glm");
	auto meta = glmTab.new_usertype<glm::vec2>
		("vec2",
		 sol::constructors<glm::vec2(), glm::vec2(float, float)>());
	
	meta["x"] = &glm::vec2::x;
	meta["y"] = &glm::vec2::y;
	meta["normalize"] = [](const glm::vec2& v){ return glm::normalize(v);};
	meta["reflect"] = [](const glm::vec2& v, const glm::vec2& n){ return glm::reflect(v,n);};
	meta["__mul"] = sol::overload([](const glm::vec2& lhs, const glm::vec2& rhs){return lhs * rhs;},
	                              [](const glm::vec2& lhs, const float& rhs){return lhs * rhs;});
	meta["__div"] = sol::overload([](const glm::vec2& lhs, const glm::vec2& rhs){return lhs / rhs;},
	                              [](const glm::vec2& lhs, const float& rhs){return lhs / rhs;});
	meta["__add"] = sol::overload([](const glm::vec2& lhs, const glm::vec2& rhs){return lhs + rhs;},
	                              [](const glm::vec2& lhs, const float& rhs){return lhs + rhs;});
	meta["__sub"] = sol::overload([](const glm::vec2& lhs, const glm::vec2& rhs){return lhs - rhs;},
	                              [](const glm::vec2& lhs, const float& rhs){return lhs - rhs;});
	meta["__tostring"] = [](const glm::vec2& v){ return glm::to_string(v);};

	glmTab.new_usertype<glm::ivec2>
		("ivec2",
		 "x", &glm::ivec2::x,
		 "y", &glm::ivec2::y);

	sol::table tab = lua.create_named_table("ybs");
	tab.new_usertype<LuaDelegate>
		("LuaDelegate",
		 "attach", &LuaDelegate::attach,
		 "detach", &LuaDelegate::detach);
	tab.new_usertype<Clock>
		("Clock",
		 "restart", &Clock::restart);
	return 0;
}

}
