#include "base/luadelegate.hpp"

namespace ybs {

void LuaDelegate::attach(sol::function f) {
	mFunctions.push_back(f);
}

bool LuaDelegate::detach(sol::function f) {
	auto it = std::find(mFunctions.begin(), mFunctions.end(), f);
	if (it != mFunctions.end()) {
		mFunctions.erase(it);
		return true;
	}
	return false;
}

}
