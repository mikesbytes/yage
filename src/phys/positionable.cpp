#include "phys/positionable.hpp"

namespace yph {

Positionable::Positionable() {
}

Positionable::Positionable(const glm::vec2& pos) :
	mPos(pos)
{
}

Positionable::Positionable(const float& x, const float& y) :
	mPos(x,y)
{
}

void Positionable::setPos(const glm::vec2 &pos) {
	mPos = pos;
}

void Positionable::setPos(const float &x, const float &y) {
	mPos = glm::vec2(x,y);
}

glm::vec2 Positionable::getPos() const {
	return mPos;
}

}
