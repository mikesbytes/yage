#include "phys/physicsworld.hpp"

#include "phys/components/position.hpp"
#include "phys/components/velocity.hpp"

namespace yph {

PhysicsWorld::PhysicsWorld() {
	mTileMapColliders.reserve(100);
	mAABBColliders.reserve(100);
}

void PhysicsWorld::update(const double& delta, entt::registry<>& registry) {
	registry.view<components::Position,
	              components::Velocity>()
		.each([delta](const auto, auto& pos, auto& vel) {
			      pos.x += vel.dX * delta;
			      pos.y += vel.dY * delta;
		      });
}

TileMapCollider& PhysicsWorld::addCollider(const TileMapCollider &col) {
	mTileMapColliders.push_back(col);
	return mTileMapColliders.back();
}

AABBCollider& PhysicsWorld::addCollider(const AABBCollider &col) {
	mAABBColliders.push_back(col);
	return mAABBColliders.back();
}

}
