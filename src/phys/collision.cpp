#include "phys/collision.hpp"

namespace yph {

Collision::Collision(const bool& collidingA, const glm::vec2& colVecA) :
	colliding(collidingA),
	colVec(colVecA)
{
}

Collision::operator bool() const {
	return colliding;
}

}
