#include "phys/scriptinterface.hpp"

#include "sol.hpp"

#include "phys/aabb.hpp"
#include "phys/collision.hpp"
#include "phys/tilemapcollider.hpp"
#include "phys/aabbcollider.hpp"
#include "phys/physicsworld.hpp"
#include "base/array2d.hpp"
#include "ecs/registercomponent.hpp"

#include "phys/components/position.hpp"
#include "phys/components/velocity.hpp"

namespace yph {

int registerScriptInterface(::sol::state& lua) {
	sol::table tab = lua.create_named_table("yph");
	tab.new_usertype<AABB>
		("AABB",
		 sol::constructors<AABB(), AABB(const glm::vec2&, const glm::vec2&)>(),
		 "contains", &AABB::contains,
		 "closestPointOnBoundsToPoint", &AABB::closestPointOnBoundsToPoint,
		 "collide", &AABB::collide);

	tab.new_usertype<Collision>
		("Collision",
		 "colliding", &Collision::colliding,
		 "colVec", &Collision::colVec);

	tab.new_usertype<Positionable>
		("Positionable",
		 "position", sol::property(&Positionable::getPos, sol::resolve<void(const glm::vec2&)>(&Positionable::setPos)));
	
	tab.new_usertype<Collider>
		("Collider",
		 "aabb", &Collider::aabb,
		 "touched", &Collider::touched);
	
	tab.new_usertype<TileMapCollider>
		("TileMapCollider",
		 sol::base_classes, sol::bases<Collider>(),
		 "collide", &TileMapCollider::collide);

	tab.new_usertype<AABBCollider>
		("AABBCollider",
		 sol::constructors<AABBCollider(const glm::vec2&, const glm::vec2&),
		 AABBCollider(const float&, const float&, const float&, const float&)>(),
		 sol::base_classes, sol::bases<Collider, Positionable>());

	tab.new_usertype<PhysicsWorld>
		("PhysicsWorld",
		 "update", &PhysicsWorld::update,
		 "addCollider", sol::overload
		 (sol::resolve<AABBCollider&(const AABBCollider&)>(&PhysicsWorld::addCollider),
		  sol::resolve<TileMapCollider&(const TileMapCollider&)>(&PhysicsWorld::addCollider)));

	tab.new_usertype<ybs::Array2D<unsigned>>
		("Tiles",
		 "shared", sol::factories([](const int& x, const int&y){
			                          return std::make_shared<ybs::Array2D<unsigned>>(x,y);}),
		 "get", &ybs::Array2D<unsigned>::get,
		 "set", &ybs::Array2D<unsigned>::set,
		 "width", &ybs::Array2D<unsigned>::width,
		 "height", &ybs::Array2D<unsigned>::height);

	{
		auto meta = yecs::registerComponent<components::Position>(lua, tab, "Position");
		meta["x"] = &components::Position::x;
		meta["y"] = &components::Position::y;
	}
	{
		auto meta = yecs::registerComponent<components::Velocity>(lua, tab, "Velocity");
		meta["dX"] = &components::Velocity::dX;
		meta["dY"] = &components::Velocity::dY;
	}

	return 0;
}

}
