#include "phys/physicssystem.hpp"

#include <iostream>

namespace yph {

PhysicsSystem::PhysicsSystem(yecs::ECS& ecs) :
	mPositions(ecs.getComponentList<Position>()),
	mAABBColliders(ecs.getComponentList<AABBCollider>())
{

}

void PhysicsSystem::update(const double &delta) {
	// mPositions is our base
	int aabbColliderIndex = 0;
	
	for (int i = 0; i < mPositions.size(); ++i) {
		unsigned id = mPositions[0].first;
		while (aabbColliderIndex < mAABBColliders.size() &&
		       mAABBColliders[aabbColliderIndex].first < id) {
			++aabbColliderIndex;
		}
		if (mAABBColliders[aabbColliderIndex].first == id) {

		}
	}
}

}
