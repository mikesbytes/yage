#include "phys/collider.hpp"

namespace yph {

Collider::Collider(const glm::vec2& pos) :
	Positionable(pos)
{
}

Collider::Collider(const float& x, const float& y) :
	Positionable(x,y)
{
}

ybs::LuaDelegate& Collider::touched() {
	return mTouched;
}

}
