#include "phys/aabbcollider.hpp"

namespace yph {

AABBCollider::AABBCollider(const glm::vec2& pos, const glm::vec2& size) :
	Collider(pos),
	mSize(size)
{
}
AABBCollider::AABBCollider(const float& x, const float& y, const float& w, const float& h) :
	Collider(x,y),
	mSize(w,h)
{

}

AABB AABBCollider::aabb() const {
	return AABB(mPos, mSize);
}

}
