#include "phys/aabb.hpp"

#include <cmath>

namespace yph {
AABB::AABB() {

}

AABB::AABB(const glm::vec2& pos, const glm::vec2& size) :
	pos(pos),
	size(size)
{

}

AABB::AABB(const float& x, const float& y, const float& w, const float& h) :
	pos(x,y),
	size(w,h)
{

}

bool AABB::contains(const glm::vec2& point) {
	return point.x > pos.x && point.x < pos.x + size.x && point.y > pos.y && point.y < pos.y + size.y;
}

glm::vec2 AABB::closestPointOnBoundsToPoint(const glm::vec2 &point) {
	float minDist = abs(point.x - pos.x);
	glm::vec2 res = glm::vec2(pos.x, point.y);
	if (abs(pos.x + size.x - point.x) < minDist) {
		minDist = abs(pos.x + size.x - point.x);
		res = glm::vec2(pos.x + size.x, point.y);
	}
	if (abs(pos.y + size.y - point.y) < minDist) {
		minDist = abs(pos.y + size.y - point.y);
		res = glm::vec2(point.x, pos.y + size.y);
	}
	if (abs(pos.y - point.y) < minDist) {
		minDist = abs(pos.y - point.y);
		res = glm::vec2(point.x, pos.y);
	}
	return res;
}

Collision AABB::collide(const AABB &box) const {
	//calculate minkowski sum of AABBs
	glm::vec2 minPos(pos.x - (box.pos.x + box.size.x),
	              pos.y - (box.pos.y + box.size.y));
	glm::vec2 minSize = size + box.size;
	AABB minkowski(minPos, minSize);

	//boxes are colliding
	if (minkowski.contains(glm::vec2(0.0f, 0.0f))) {
		return Collision(true, minkowski.closestPointOnBoundsToPoint(glm::vec2(0.0f,0.0f)));
	}
	return Collision(false, glm::vec2(0,0));
}



}
