#include "phys/tilemapcollider.hpp"
#include "phys/aabb.hpp"
#include "base/math.hpp"

#include <cmath>
#include <algorithm>

namespace yph {

TileMapCollider::TileMapCollider(std::shared_ptr<ybs::Array2D<unsigned>> tiles, const glm::vec2& pos, const float& tileSize) :
	mTiles(tiles),
	Collider(pos),
	mTileSize(tileSize)
{

}

Collision TileMapCollider::collide(const AABB &box) {
	AABB resolved(box);
	Collision col(false, glm::vec2(0.0f));

	// find tiles overlapping the AABB
	int tBoxL = floor((box.pos.x - mPos.x) / mTileSize); // left
	int tBoxB = floor((box.pos.y - mPos.y) / mTileSize); // bottom
	int tBoxR = ceil((box.pos.x + box.size.x) / mTileSize);     // right
	int tBoxT = ceil((box.pos.y + box.size.y) / mTileSize);     // top

	// clamp selection to the tilemap
	tBoxL = std::max(tBoxL, 0);
	tBoxB = std::max(tBoxB, 0);
	tBoxR = std::min(tBoxR, mTiles->width());
	tBoxT = std::min(tBoxT, mTiles->height());

	for (int y = tBoxB; y <= tBoxT; ++y) {
		for (int x = tBoxL; x <= tBoxR; ++x) {
			if (mTiles->get(x, y) != 0) { // tile is solid
				// build an AABB matching the tile
				AABB tile(mPos.x + (float)x * mTileSize,
				          mPos.y + (float)y * mTileSize,
				          mTileSize, mTileSize);
				auto tCol = tile.collide(resolved);

				// correct the resolution box
				if (tCol.colliding) {
					col.colliding = true;
					resolved.pos += tCol.colVec;
					col.colVec = resolved.pos - box.pos;
				}
			}
		}
	}

	return col;
}

AABB TileMapCollider::aabb() const {
	return AABB(mPos.x, mPos.y,
	            mTileSize * mTiles->width(),
	            mTileSize * mTiles->height());
}

}
