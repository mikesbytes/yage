#include "graphics/bitmap.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

namespace ygp {

Bitmap::Bitmap(const std::string& fileName) {
	stbi_set_flip_vertically_on_load(true);
	mData = stbi_load(fileName.c_str(),
	                  &mWidth,
	                  &mHeight,
	                  &mChannelCount,
	                  STBI_rgb_alpha);
}

Bitmap::~Bitmap() {
	if (mData) {
		stbi_image_free(mData);
		mData = nullptr;
	}
}

int Bitmap::width() const {
	return mWidth;
}

int Bitmap::height() const {
	return mHeight;
}

unsigned char* Bitmap::data() const {
	return mData;
}

}
