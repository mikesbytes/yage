#include "graphics/scriptinterface.hpp"
#include "graphics/inputhandler.hpp"
#include "graphics/window.hpp"
#include "graphics/camera.hpp"
#include "graphics/quad.hpp"
#include "graphics/tilemap.hpp"
#include "graphics/bitmap.hpp"
#include "graphics/arraytexture.hpp"
#include "base/array2d.hpp"
#include "base/resourcemanager.hpp"
#include "sol.hpp"

#include "ecs/registercomponent.hpp"

#include <SDL2/SDL.h>


namespace ygp {

int registerScriptInterface(::sol::state& lua) {
	lua["SDL"] = lua.create_table_with
		("QUIT", SDL_QUIT,
		 "MOUSEMOTION", SDL_MOUSEMOTION);

	sol::table tab = lua.create_named_table("ygp");
	tab.new_usertype<Window>
		("Window",
		 "create", &Window::create,
		 "title", sol::property(&Window::getTitle, &Window::setTitle),
		 "clear", &Window::clear,
		 "swap", &Window::swap,
		 "size", sol::property(&Window::getSize, &Window::setSize));

	tab.new_usertype<Camera>
		("Camera",
		 sol::constructors<Camera(), Camera(const float& x, const float& y, const float& w, const float& h)>(),
		 "position", sol::property(&Camera::getPos, &Camera::setPos));

	tab.new_usertype<InputHandler>
		("InputHandler",
		 sol::constructors<InputHandler(Window&)>(),
		 "process", &InputHandler::process,
		 "getEventDelegate", &InputHandler::getEventDelegate,
		 "isKeyPressed", &InputHandler::isKeyPressed);

	tab.new_usertype<Quad>
		("Quad",
		 sol::constructors<Quad(), Quad(const float&, const float&, const float&, const float&)>(),
		 "draw", &Quad::draw,
		 "size", sol::property(&Quad::getSize, &Quad::setSize),
		 "position", sol::property(&Quad::getPosition, &Quad::setPosition),
		 "setFollow", &Quad::setFollow,
		 "aabb", &Quad::aabb);

	auto tileMapMeta =
		yecs::registerComponentArgs<TileMap,
		                            const float&, const float&, const float&,
		                            const int&, const int&,
		                            const std::shared_ptr<ArrayTexture>&,
		                            const std::shared_ptr<ybs::Array2D<unsigned>>&>
		(lua, tab, "TileMap");

	tileMapMeta["draw"] = &TileMap::draw;
	tileMapMeta["setTile"] = &TileMap::setTile;
	tileMapMeta["getTile"] = &TileMap::rebuildMesh;
	tileMapMeta["collider"] = &TileMap::collider;

	tab.new_usertype<ArrayTexture>
		("ArrayTexture",
		 sol::constructors<ArrayTexture(const int&, const int&)>(),
		 "setTextureAt", &ArrayTexture::setTextureAt,
		 "bind", &ArrayTexture::bind);

	tab.new_usertype<Bitmap>
		("Bitmap",
		 sol::constructors<Bitmap(const std::string&)>(),
		 "width", &Bitmap::width,
		 "height", &Bitmap::height,
		 "data", &Bitmap::data);

	tab.new_usertype<ybs::ResourceManager<ArrayTexture>>
		("ArrayTextureManager",
		 "addResource", &ybs::ResourceManager<ArrayTexture>::addResource,
		 "newResource", &ybs::ResourceManager<ArrayTexture>::
		 newResource<const int&, const int&>,
		 "removeResource", &ybs::ResourceManager<ArrayTexture>::addResource,
		 "getResource", &ybs::ResourceManager<ArrayTexture>::getResource);
	return 0;
}

}
