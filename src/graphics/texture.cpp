#include "graphics/texture.hpp"


//lib includes
#define GLEW_STATIC
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#include "graphics/bitmap.hpp"

namespace ygp {

Texture::Texture(const Bitmap& bitmap) :
	mWidth(bitmap.width()),
	mHeight(bitmap.height())
{
	glGenTextures(1, &mTexture);
	glBindTexture(GL_TEXTURE_2D, mTexture);

	//set tex parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	
	glTexImage2D(GL_TEXTURE_2D,
	             0,
	             GL_RGBA,
	             bitmap.width(),
	             bitmap.height(),
	             0,
	             GL_RGBA,
	             GL_UNSIGNED_BYTE,
	             bitmap.data());

}

Texture::~Texture() {
	glDeleteTextures(1, &mTexture);
}

void Texture::bind() {
	glBindTexture(GL_TEXTURE_2D, mTexture);
}

int Texture::width() {
	return mWidth;
}

int Texture::height() {
	return mHeight;
}

}
