#include "graphics/window.hpp"
#include <SDL2/SDL.h>
#define GLEW_STATIC
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

namespace ygp {

Window::Window() :
	mActive(false),
	mWindow(nullptr),
	mTitle("YAGE"),
	mSize(1280,720),
	mPosition(0,0)
{

}

Window::~Window() {
	if (mActive) {
		SDL_DestroyWindow((SDL_Window*)mWindow);
		SDL_GL_DeleteContext(*(SDL_GLContext*) mContext);
		SDL_Quit();
	}
}

void Window::create() {
	if (mActive) return;

	mActive = true;

	SDL_Init(SDL_INIT_VIDEO);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
	
	mWindow = SDL_CreateWindow(mTitle.c_str(),
	                           mPosition.x, mPosition.y,
	                           mSize.x, mSize.y,
	                           SDL_WINDOW_OPENGL);

	mContext = SDL_GL_CreateContext((SDL_Window*)mWindow);

	glewExperimental = GL_TRUE;
	glewInit();
	
}

void Window::clear() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Window::swap() {
	if (mActive) {
		SDL_GL_SwapWindow((SDL_Window*)mWindow);
	}
}

void Window::setTitle(const std::string& title) {
	mTitle = title; 

	//change title of the window if it's active
	if (mActive) {
		SDL_SetWindowTitle((SDL_Window*)mWindow, mTitle.c_str());
	}
}

std::string Window::getTitle() {
	return mTitle;
}

void Window::setSize(const glm::ivec2& size) {
	mSize = size;
}

glm::ivec2 Window::getSize() {
	// if active get size from the window instead of mSize
	if (mActive) {
		glm::ivec2 size;
		SDL_GetWindowSize((SDL_Window*)mWindow,
		                  &size.x,
		                  &size.y);
		return size;
	}
	return mSize;
}

}
