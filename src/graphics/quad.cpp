#include "graphics/quad.hpp"
#define GLEW_STATIC
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#include <iostream>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "graphics/camera.hpp"
#include "base/resourcemanager.hpp"
#include "graphics/mesh.hpp"
#include "graphics/bitmap.hpp"

namespace ygp {

Quad::Quad():
	mTex(Bitmap("share/exclamation.png")),
	mFollow(nullptr)
{}

Quad::Quad(const float& x, const float& y, const float& w, const float& h) :
	mSize(w,h),
	mPos(x,y),
	mTex(Bitmap("share/exclamation.png")),
	mFollow(nullptr)
{
	ybs::ResourceManager<ShaderProgram> rm;
	mShaderProg = rm.getResource("simple2d");
	if (mShaderProg == nullptr) {
		mShaderProg = std::make_shared<ShaderProgram>("share/simple2d.vert.glsl",
		                                              "share/simple2d.frag.glsl");
		rm.addResource("simple2d", mShaderProg);
	}
	mShader = mShaderProg->id();
	mShaderProg->use();

	mModelUni = glGetUniformLocation(mShader, "model");
	mViewUni = glGetUniformLocation(mShader, "view");
	glBindFragDataLocation(mShader, 0, "outColor");

	std::vector<float> verts = { 0.0f, 0.0f, 0.0f, 0.0f,
	                             0.0f, 1.0f, 0.0f, 1.0f,
	                             1.0f, 1.0f, 1.0f, 1.0f,
	                             0.0f, 0.0f, 0.0f, 0.0f,
	                             1.0f, 1.0f, 1.0f, 1.0f,
	                             1.0f, 0.0f, 1.0f, 0.0f };

	mMesh.setBufferData<0>(verts);
}

void Quad::draw(Camera& cam) {
	glUseProgram(mShader);
	mTex.bind();
	//update view mat
	auto view = cam.getViewMat();
	glUniformMatrix4fv(mViewUni, 1, GL_FALSE, glm::value_ptr(view));
	updateModelMat();

	mMesh.drawCall();
}

void Quad::setSize(const glm::vec2& size) {
	mSize = size;
	updateModelMat();
}

glm::vec2 Quad::getSize() {
	return mSize;
}

void Quad::setPosition(const glm::vec2 &pos) {
	mPos = pos;
	updateModelMat();
}

glm::vec2 Quad::getPosition() {
	return mPos;
}

void Quad::setFollow(yph::Positionable *arg) {
	mFollow = arg;
}

yph::AABB Quad::aabb() {
	return yph::AABB(mPos, mSize);
}

void Quad::updateModelMat() {
	auto pos = mPos;
	if (mFollow != nullptr) {
		pos = mFollow->getPos() + mPos;
	}

	glm::mat4 model = 
		glm::translate(glm::mat4(1.0f), glm::vec3(pos.x, pos.y, 0.0f)) *
		glm::scale(glm::mat4(1.0f), glm::vec3(mSize.x, mSize.y, 1.0f));
	glUniformMatrix4fv(mModelUni, 1, GL_FALSE, glm::value_ptr(model));
}

}
