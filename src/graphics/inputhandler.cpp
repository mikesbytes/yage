#include "graphics/inputhandler.hpp"

#include <SDL2/SDL.h>

namespace ygp {

InputHandler::InputHandler(Window& window) :
	mWindow(window)
{

}

void InputHandler::process() {
	SDL_Event ev;
	while (SDL_PollEvent(&ev)) {
		// get the list of callbacks for an event
		auto it = mEventDelegates.find(ev.type);
		if (it != mEventDelegates.end()) {
			it->second.call();
		}
	}
	mKeys = SDL_GetKeyboardState(NULL);
}

ybs::LuaDelegate& InputHandler::getEventDelegate(const uint32_t &ev) {
	return mEventDelegates[ev];
}

bool InputHandler::isKeyPressed(const std::string &keyName) {
	SDL_Keycode identifier = SDL_GetKeyFromName(keyName.c_str());
	return mKeys[SDL_GetScancodeFromKey(identifier)];
}

}
