#include "graphics/camera.hpp"

#include <glm/gtc/matrix_transform.hpp>

namespace ygp {

Camera::Camera() {

}

Camera::Camera(const float& x, const float& y, const float& w, const float& h) :
	mPos(x,y),
	mSize(w,h)
{

}

glm::vec2 Camera::getPos() {
	return mPos;
}

void Camera::setPos(const glm::vec2 &pos) {
	mPos = pos;
}

glm::mat4 Camera::getViewMat() {
	return glm::ortho(mPos.x,
	                  mPos.x + mSize.x,
	                  mPos.y,
	                  mPos.y + mSize.y,
	                  -1.0f, 1.0f);
}

}
