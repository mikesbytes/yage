#include "graphics/shaderprogram.hpp"

#define GLEW_STATIC
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <fstream>
#include <algorithm>

#include "base/filesystem.hpp"

namespace ygp {

ShaderUniform::ShaderUniform(const ShaderUniform& u) :
	mUni(u.mUni)
{
}

ShaderUniform::ShaderUniform() {}

ShaderUniform::ShaderUniform(const int& uni) :
	mUni(uni)
{
}


void ShaderUniform::setValue(const glm::mat4 &value) {
	glUniformMatrix4fv(mUni, 1, GL_FALSE, glm::value_ptr(value));
}

void ShaderUniform::setUniform(const int &uni) {
	mUni = uni;
}

ShaderProgram::ShaderProgram(const std::string& vertexPath, const std::string& fragmentPath) {
	ybs::FileSystem f;
	
    // Create the shaders
    GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
 
    // Read the Vertex Shader code from the file
    std::string VertexShaderCode = f.getFileAsString(vertexPath);
 
    // Read the Fragment Shader code from the file
    std::string FragmentShaderCode = f.getFileAsString(fragmentPath);
 
    GLint Result = GL_FALSE;
    int InfoLogLength;
 
    // Compile Vertex Shader
    char const * VertexSourcePointer = VertexShaderCode.c_str();
    glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
    glCompileShader(VertexShaderID);
 
    // Check Vertex Shader
    glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    std::vector<char> VertexShaderErrorMessage(InfoLogLength);
    glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
    fprintf(stdout, "%s\n", &VertexShaderErrorMessage[0]);
 
    // Compile Fragment Shader
    char const * FragmentSourcePointer = FragmentShaderCode.c_str();
    glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
    glCompileShader(FragmentShaderID);
 
    // Check Fragment Shader
    glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    std::vector<char> FragmentShaderErrorMessage(InfoLogLength);
    std::string errorMsg;
    errorMsg.reserve(InfoLogLength);
    glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, errorMsg.data());
    fprintf(stdout, "%s\n", errorMsg.c_str());
 
    // Link the program
    fprintf(stdout, "Linking program\n");
    mShader = glCreateProgram();
    glAttachShader(mShader, VertexShaderID);
    glAttachShader(mShader, FragmentShaderID);
    glLinkProgram(mShader);
 
    // Check the program
    glGetProgramiv(mShader, GL_LINK_STATUS, &Result);
    glGetProgramiv(mShader, GL_INFO_LOG_LENGTH, &InfoLogLength);
    std::vector<char> ProgramErrorMessage( std::max(InfoLogLength, int(1)) );
    glGetProgramInfoLog(mShader, InfoLogLength, NULL, &ProgramErrorMessage[0]);
    fprintf(stdout, "%s\n", &ProgramErrorMessage[0]);
 
    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);

	glBindFragDataLocation(mShader, 0, "outColor");

	GLint posAttrib = glGetAttribLocation(mShader, "position");
	glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(posAttrib);
}

ShaderUniform ShaderProgram::getUniform(const std::string &name) {
	return ShaderUniform(glGetUniformLocation(mShader, name.c_str()));
}

void ShaderProgram::use() {
	glUseProgram(mShader);
}

unsigned ShaderProgram::id() {
	return mShader;
}

}
