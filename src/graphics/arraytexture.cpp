#include "graphics/arraytexture.hpp"

#include <SDL2/SDL_opengl.h>

#include "graphics/bitmap.hpp"

namespace ygp {

ArrayTexture::ArrayTexture(const int& tileSize, const int& tileCount) :
	mTileSize(tileSize),
	mTileCount(tileCount)
{
	glGenTextures(1, &mTexture);
	glActiveTexture(GL_TEXTURE0);

	glBindTexture(GL_TEXTURE_2D_ARRAY, mTexture);
	// create empty array texture
	glTexImage3D(GL_TEXTURE_2D_ARRAY,
	             0, GL_RGBA,
	             tileSize, tileSize,
	             tileCount,
	             0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glTexParameteri (GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri (GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri (GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri (GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
}

void ArrayTexture::setTextureAt(const int& index, const Bitmap& bitmap) {
	glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, index,
	                mTileSize, mTileSize, 1, GL_RGBA, GL_UNSIGNED_BYTE, bitmap.data());
}

void ArrayTexture::bind() {
	glBindTexture(GL_TEXTURE_2D_ARRAY, mTexture);
}

}
