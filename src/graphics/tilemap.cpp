#include "graphics/tilemap.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "graphics/bitmap.hpp"
#include "base/resourcemanager.hpp"

namespace ygp {

TileMap::TileMap(const float& tileSize, const float& x, const float& y,
                 const int& mapWidth, const int& mapHeight,
                 const std::shared_ptr<ArrayTexture>& tex,
                 const std::shared_ptr<ybs::Array2D<unsigned>>& tiles) :
	mTex(tex),
	mTileSize(tileSize),
	mPos(x,y),
	mMapWidth(mapWidth),
	mMapHeight(mapHeight),
	mTiles(tiles)
{
	// load shader
	ybs::ResourceManager<ShaderProgram> rm;
	mShader = rm.getResource("tilemap");
	if (mShader == nullptr) {
		mShader = std::make_shared<ShaderProgram>("share/tilemap.vert.glsl",
		                                          "share/tilemap.frag.glsl");
		rm.addResource("tilemap", mShader);
	}
	mShader->use();
	mModelUni = glGetUniformLocation(mShader->id(), "model");
	mViewUni = glGetUniformLocation(mShader->id(), "view");
	glBindFragDataLocation(mShader->id(), 0, "outColor");


	// model matrix TODO: move this elsewhere
	glm::mat4 model =
		glm::translate(glm::mat4(1.0f), glm::vec3(mPos.x, mPos.y, 0.0f)) *
		glm::scale(glm::mat4(1.0f), glm::vec3(mTileSize, mTileSize, 1.0f));
	glUniformMatrix4fv(mModelUni, 1, GL_FALSE, glm::value_ptr(model));
}

TileMap::TileMap(TileMap&& other) :
	mTiles(other.mTiles),
	mShader(other.mShader),
	mModelUni(other.mModelUni),
	mViewUni(other.mViewUni),
	mTex(other.mTex),
	mTileSize(other.mTileSize),
	mPos(other.mPos),
	mMapWidth(other.mMapWidth),
	mMapHeight(other.mMapHeight)
{
	
}

TileMap& TileMap::operator=(const TileMap& other) {
	mTiles = other.mTiles;
	mShader = other.mShader;
	mModelUni = other.mModelUni;
	mViewUni = other.mViewUni;
	mTex = other.mTex;
	mTileSize = other.mTileSize;
	mPos = other.mPos;
	mMapWidth = other.mMapWidth;
	mMapHeight = other.mMapHeight;
	return *this;
}

void TileMap::draw(Camera& cam) {
	glUseProgram(mShader->id());
	mTex->bind();
	auto view = cam.getViewMat();
	glUniformMatrix4fv(mViewUni, 1, GL_FALSE, glm::value_ptr(view));
	mTileMesh.drawCall();
}

unsigned TileMap::setTile(const int &x, const int &y, const unsigned &value) {
	unsigned ret = mTiles->get(x,y);
	mTiles->set(x,y,value);
	return ret;
}

unsigned TileMap::getTile(const int& x, const int& y) {
	return mTiles->get(x,y);
}

void TileMap::rebuildMesh() {
	std::vector<float> verts;
	auto iFunc = [&verts](const int& xI, const int& yI, const unsigned& value) {
		if (value == 0) return;
		//get x and y tile positions from index
		float x = (float)xI;
		float y = (float)yI;
		//save offsets for more easy doing of things
		float xo = x + 1;
		float yo = y + 1;
		float texIndex = (float)(value - 1);
		//vertices for current tile
		//                           x,  y,  u,    v,    i
		std::vector<float> tileV = { x,  y,  0.0f, 0.0f, texIndex,
		                             x,  yo, 0.0f, 1.0f, texIndex,
		                             xo, yo, 1.0f, 1.0f, texIndex,
		                             x,  y,  0.0f, 0.0f, texIndex,
		                             xo, yo, 1.0f, 1.0f, texIndex,
		                             xo, y,  1.0f, 0.0f, texIndex };
		verts.insert(verts.end(), tileV.begin(), tileV.end());
	};
	mTiles->iterate(iFunc);
	mTileMesh.setBufferData<0>(verts);
}

yph::TileMapCollider TileMap::collider() {
	return yph::TileMapCollider(mTiles, mPos, mTileSize);
}


}
