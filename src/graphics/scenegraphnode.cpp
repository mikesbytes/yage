//
// Created by user on 2/2/19.
//


#include "graphics/scenegraphnode.hpp"

namespace ygp {

SceneGraphNode::SceneGraphNode() :
		mParent(nullptr),
		mFirstChild(nullptr),
		mLastChild(nullptr),
		mPrevSibling(nullptr),
		mNextSibling(nullptr)
{}

SceneGraphNode::~SceneGraphNode() {
	// fix parent's first/last child if necessary
	if (mParent != nullptr && mParent->mFirstChild == this) {
		mParent->mFirstChild = mNextSibling;
	} else if (mParent != nullptr && mParent->mLastChild == this) {
		mParent->mLastChild = mPrevSibling;
	}

	// fix siblings
	if (mPrevSibling != nullptr) {
		mPrevSibling->mNextSibling = mNextSibling;
	}
	if (mNextSibling != nullptr) {
		mNextSibling->mPrevSibling = mPrevSibling;
	}

	// delete children
	auto search = mFirstChild;
	while(search != nullptr) {
		// store the current search for later deletion
		// This cannot be done by incrementing then using the previous sibling
		// because the next sibling may be nullptr
		auto tmp = search;
		search = tmp->mNextSibling;
		delete(tmp);
	}
}

void SceneGraphNode::addChild(SceneGraphNode* node) {
	node->mParent = this;
	node->mPrevSibling = mLastChild;
	if (mLastChild != nullptr) {
		mLastChild->mNextSibling = node;

	}
	if (mFirstChild == nullptr) {
		mFirstChild = node;
	}
	mLastChild = node;
}

SceneGraphNode *ygp::SceneGraphNode::getParent() const {
	return mParent;
}

SceneGraphNode *ygp::SceneGraphNode::getNextSibling() const {
	return mNextSibling;
}

SceneGraphNode *ygp::SceneGraphNode::getFirstChild() const {
	return mFirstChild;
}

SceneGraphNode *ygp::SceneGraphNode::getLastChild() const {
	return mLastChild;
}

SceneGraphNode *ygp::SceneGraphNode::getPrevSibling() const {
	return mPrevSibling;
}



const glm::mat4 &SceneGraphNode::getTransform() const {
	return mTransform;
}

void SceneGraphNode::setTransform(const glm::mat4 &mTransform) {
	SceneGraphNode::mTransform = mTransform;
}

int SceneGraphNode::childCount() {
	int count = 0;
	for (
			auto search = mFirstChild;
			search != nullptr;
			search = search->mNextSibling
	) {
		++count;
	}
	return count;
}

}
