#include <iostream>

#include "ecs/scriptinterface.hpp"
#include "graphics/scriptinterface.hpp"
#include "base/scriptinterface.hpp"
#include "phys/scriptinterface.hpp"
#include "sol.hpp"

int main(int argc, char* argv[]) {
	sol::state lua;
	lua.open_libraries(sol::lib::base, sol::lib::package, sol::lib::math, sol::lib::table);

	//register lua interfaces
	yecs::registerScriptInterface(lua);
	ybs::registerScriptInterface(lua);
	yph::registerScriptInterface(lua);
	ygp::registerScriptInterface(lua);
	if (argc == 2) {
		lua.script_file(argv[1]);
	} else {
		lua.script_file("share/testscript.lua");
	}

	return 0;
}
