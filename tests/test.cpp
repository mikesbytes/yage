#include "base/filesystem.hpp"
#include "graphics/mesh.hpp"
#include "ecs/componentlist.hpp"
#include "ecs/ecs.hpp"
#include "phys/physicssystem.hpp"
#include "ecs/registercomponent.hpp"
#include "graphics/scenegraphnode.hpp"
#include "sol.hpp"

#include <iostream>

#define CATCH_CONFIG_MAIN
#include "catch.hpp"

TEST_CASE( "File can be read into string", "[FileSystem]" ) {
	ybs::FileSystem fs;

	std::string comp_string = "Hellö,\nWorld!\n";
	
	REQUIRE( fs.getFileAsString("testdata/hello.txt") == comp_string );
}

TEST_CASE( "BufferFormat is compliant", "[BufferFormat]" ) {
	typedef ygp::BufferFormat<float, 2, 2> bf;

	REQUIRE( bf::size() == 4 );
	REQUIRE( bf::spanCount() == 2);
}

TEST_CASE( "Correct ComponentList insert behavior", "[ComponentList]" ) {
	yecs::ECS ecs;
	auto& list = ecs.getComponentList<int>();

	int three = 3;
	int one = 1;
	list.addComponent(1, three);
	list.addComponent(0, one);

	REQUIRE( list.getComponentList()[0].first == 0 );
}

TEST_CASE( "Test SceneGraphNode", "[SceneGraphNode]" ) {
	auto root = new ygp::SceneGraphNode();
	auto child = new ygp::SceneGraphNode();

	REQUIRE(root->childCount() == 0);

	root->addChild(child);
	REQUIRE(root->getFirstChild() == child);
	REQUIRE(root->getLastChild() == child);
	REQUIRE(child->getParent() == root);
	REQUIRE(root->childCount() == 1);

	auto child2 = new ygp::SceneGraphNode();
	root->addChild(child2);
	REQUIRE(root->getFirstChild() == child);
	REQUIRE(root->getLastChild() == child2);
	REQUIRE(child2->getPrevSibling() == child);
	REQUIRE(child2->getNextSibling() == nullptr);
	REQUIRE(root->childCount() == 2);

	delete(child);
	REQUIRE(root->getFirstChild() == child2);
	REQUIRE(child2->getPrevSibling() == nullptr);
	REQUIRE(root->childCount() == 1);
}
