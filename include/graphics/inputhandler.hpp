#pragma once

#include <mutex>
#include <functional>
#include <map>
#include <vector>
#include <cstdint>

#include "graphics/window.hpp"
#include "base/luadelegate.hpp"
#include "sol.hpp"

namespace ygp {

class InputHandler {
public:
	InputHandler(Window& window);
	
	void process();
	ybs::LuaDelegate& getEventDelegate(const uint32_t& ev);
	bool isKeyPressed(const std::string& keyName);

protected:
	//TODO: switch this to use some sort of context system
	std::map<uint32_t, ybs::LuaDelegate> mEventDelegates;
	Window& mWindow;
	const uint8_t* mKeys;
};

}
