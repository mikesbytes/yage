#pragma once

namespace ygp {

//forward declarations
class Bitmap;

class Texture {
public:
	Texture(const Bitmap& bitmap);
	~Texture();

	void bind();
	int width();
	int height();
protected:
	unsigned mTexture;
	int mWidth;
	int mHeight;
};

}
