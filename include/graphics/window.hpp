#pragma once

#include <string>
#include <glm/glm.hpp>

namespace ygp {

class InputHandler;

class Window {
	friend class InputHandler;
public:
	Window();
	~Window();

	void create();
	void setTitle(const std::string& title);
	void clear();
	void swap();
	std::string getTitle();

	void setSize(const glm::ivec2& size);
	glm::ivec2 getSize();

protected:
	bool mActive;
	void* mWindow;
	void* mContext;

	glm::ivec2 mSize;
	glm::ivec2 mPosition;

	std::string mTitle;
	
};

}
