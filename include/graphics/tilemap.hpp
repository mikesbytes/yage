#pragma once

#include <memory>

#include "graphics/mesh.hpp"
#include "graphics/texture.hpp"
#include "graphics/shaderprogram.hpp"
#include "graphics/arraytexture.hpp"
#include "base/array2d.hpp"

#include "phys/tilemapcollider.hpp"

namespace ygp {

class ShaderProgram;
class Camera;

class TileMap {
public:
	TileMap(const float& tileSize, const float& x, const float& y,
	        const int& mapWidth, const int& mapHeight, const std::shared_ptr<ArrayTexture>& tex,
	        const std::shared_ptr<ybs::Array2D<unsigned>>& tiles);
	TileMap(TileMap&& other);
	TileMap& operator=(const TileMap& other);

	void draw(Camera& cam);
	unsigned setTile(const int& x, const int& y, const unsigned& value);
	unsigned getTile(const int& x, const int& y);
	void rebuildMesh();

	yph::TileMapCollider collider();
protected:

	Mesh<BufferFormat<float,2,3>> mTileMesh;
	//std::vector<int> mTiles;
	std::shared_ptr<ybs::Array2D<unsigned>> mTiles;
	std::shared_ptr<ShaderProgram> mShader;

	int mModelUni, mViewUni;

	std::shared_ptr<ArrayTexture> mTex;
	float mTileSize;
	glm::vec2 mPos;
	int mMapWidth, mMapHeight;
};

}
