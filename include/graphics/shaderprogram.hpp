#pragma once

#include <string>
#include <glm/glm.hpp>
#include <memory>

namespace ygp {

class ShaderUniform {
public:
	ShaderUniform(const ShaderUniform& u);
	ShaderUniform();
	ShaderUniform(const int& uni);
	void setValue(const glm::mat4& value);
	void setUniform(const int& uni);

	int mUni; 
};

class ShaderProgram {
public:
	ShaderProgram(const std::string& fs, const std::string& vs);
	ShaderUniform getUniform(const std::string& name);

	void use();
	unsigned id();
	
protected:
	unsigned mShader;
};

int LoadShaders(const char* vertex_file_path, const char* fragment_file_path);

}
