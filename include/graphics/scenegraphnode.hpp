#pragma once

#include <glm/glm.hpp>

namespace ygp {

class SceneGraphNode {
public:
	SceneGraphNode();
	virtual ~SceneGraphNode();

	void addChild(SceneGraphNode* node);
	int childCount();

	SceneGraphNode *getParent() const;
	SceneGraphNode *getFirstChild() const;
	SceneGraphNode *getLastChild() const;
	SceneGraphNode *getPrevSibling() const;
	SceneGraphNode *getNextSibling() const;

	const glm::mat4 &getTransform() const;
	void setTransform(const glm::mat4 &mTransform);

protected:
	SceneGraphNode *mParent;
	SceneGraphNode *mFirstChild, *mLastChild;
	SceneGraphNode *mPrevSibling, *mNextSibling;

	glm::mat4 mTransform;
};

}