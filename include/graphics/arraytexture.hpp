#pragma once

namespace ygp {

class Bitmap;

class ArrayTexture {
public:
	ArrayTexture(const int& tileSize, const int& tileCount);

	void setTextureAt(const int& index, const Bitmap& bitmap);
	void bind();
protected:
	int mTileSize, mTileCount;
	unsigned mTexture;
};

}
