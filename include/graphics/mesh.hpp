#pragma once

#include <tuple>
#include <vector>
#include <iostream>

#define GLEW_STATIC
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#include "base/tupleforeach.hpp"
#include "graphics/camera.hpp"

namespace ygp {

/// Generic OpenGL Mesh Class
/**
 * Allows you to define arbitrarily formatted meshes with multiple
 * VBOs, interleaved data, etc.
 *
 * Ex: `Mesh<BufferFormat<float, 3, 2>>` would create a mesh with a single
 * VBO with a vec3 in the 0 attrib and vec2 in 1, such as with 3d points
 * and tex coords. An equivelant with 2 VBOs could be made with
 * `Mesh<BufferFormat<float, 3>, BufferFormat<float, 2>>`
 */
template <typename T, int... Spans>
struct BufferFormat {
	typedef T dataType;
	static constexpr int size() {
		int size = 0;
		auto tup = std::tuple(Spans...);

		ybs::forEachInTuple(tup, [&size](const auto& i) {
			                         size += i;
		                         });
		return size;
	}

	static constexpr int typeSize() { return sizeof(T); }
	
	static constexpr int spanCount() { return sizeof...(Spans); }

	template<int Index>
	static constexpr int getSpan() {
		auto tup = std::tuple(Spans...);
		return std::get<Index>(tup);
	}

	static constexpr auto getSpans() {
		auto tup = std::tuple(Spans...);
		return tup;
	}
};

template<typename... Formats>
class Mesh {
public:
	Mesh() :
		vCount(0)
	{
		glGenVertexArrays(1, &mVAO);
		glBindVertexArray(mVAO);

		int index = 0;
		for (int i = 0; i < sizeof...(Formats); ++i) {
			glGenBuffers(1, &mVBOs[i]);
			glBindBuffer(GL_ARRAY_BUFFER, mVBOs[i]);
		}
		
		//auto x = {(genVBO<Formats>(index),0)...};
	}

	~Mesh() {
		//delete buffers
		glDeleteBuffers(6, &mVBOs[0]);
		glDeleteVertexArrays(1, &mVAO);
	}

	template<int index, typename T>
	void setBufferData(std::vector<T>& data) {
		auto tup = std::tuple<Formats...>();
		vCount = data.size() / std::get<index>(tup).size();
		glBindVertexArray(mVAO);
		glBindBuffer(GL_ARRAY_BUFFER, mVBOs[index]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(T) * data.size(), data.data(), GL_STATIC_DRAW);
		genVBO(std::get<index>(tup), index);
	}

	void drawCall() {
		glBindVertexArray(mVAO);
		glDrawArrays(GL_TRIANGLES, 0, vCount);
	}

protected:
	Mesh(const Mesh<Formats...>& other);
	
	template<typename T>
	inline void genVBO(const T& format, int ndex) {
		auto tup = T::getSpans();
		unsigned pCount = 0;
		int start = 0;
		ybs::forEachInTuple(tup,
		                    [&pCount, &start](const auto& span) {
			                    glVertexAttribPointer(pCount,
			                                          span,
			                                          GL_FLOAT,
			                                          GL_FALSE,
			                                          T::typeSize() * T::size(),
			                                          reinterpret_cast<void*>(T::typeSize() * start));
			                    glEnableVertexAttribArray(pCount);
			                    start += span;
			                    ++pCount;
		                    });
		
	}

	int vCount;
	GLuint mVAO;
	std::array<GLuint, sizeof...(Formats)> mVBOs;
};


}
