#pragma once

namespace ygp {

class Camera;

class SceneGraph {
public:
	void draw(Camera& cam);
};

}