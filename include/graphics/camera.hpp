#pragma once

#include <glm/glm.hpp>

namespace ygp {

class Camera {
public:
	Camera();
	Camera(const float& x, const float& y, const float& w, const float& h);

	glm::vec2 getPos();
	void setPos(const glm::vec2& pos);

	glm::mat4 getViewMat();

protected:
	glm::vec2 mPos;
	glm::vec2 mSize;
};

}
