#pragma once

#include <memory>

#include "phys/positionable.hpp"
#include "graphics/texture.hpp"

namespace ygp {

class Camera;

class Sprite : public yph::Positionable
{
public:
	Sprite(std::shared_ptr<Texture> tex);

	void draw(Camera& cam);
protected:
	std::shared_ptr<Texture> mTex;
};

}
