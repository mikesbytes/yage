#pragma once

#include <string>
#include <vector>

namespace ygp {

class Bitmap {
public:
	Bitmap(const std::string& fileName);
	~Bitmap();

	int width() const;
	int height() const;
	unsigned char* data() const;
protected:
	Bitmap(const Bitmap& other);
	int mWidth;
	int mHeight;
	int mChannelCount;
	unsigned char* mData;
};

}
