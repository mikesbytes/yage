#pragma once

#include <glm/glm.hpp>
#include <memory>

#include "phys/aabb.hpp"
#include "phys/positionable.hpp"
#include "graphics/shaderprogram.hpp"
#include "graphics/mesh.hpp"
#include "graphics/texture.hpp"

namespace ygp {

//forward declarations
class Camera;

class Quad {
public:
	Quad();
	Quad(const float& x, const float& y, const float& w, const float& h);

	void draw(Camera& cam);

	void setSize(const glm::vec2& size);
	glm::vec2 getSize();

	void setPosition(const glm::vec2& pos);
	glm::vec2 getPosition();

	void setFollow(yph::Positionable* arg);

	yph::AABB aabb();

protected:
	void updateModelMat();

	Mesh<BufferFormat<float, 2, 2>> mMesh;
	Texture mTex;

	// lock position to a positionable
	yph::Positionable* mFollow;

	glm::vec2 mSize;
	glm::vec2 mPos;

	//shader data
	std::shared_ptr<ShaderProgram> mShaderProg;
	unsigned mShader;
	int mModelUni;
	int mViewUni;
};
}
