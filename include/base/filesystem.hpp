#pragma once

#include <vector>
#include <cstdint>
#include <string>

namespace ybs {

/// Handles all file loading and management
/**
 * Contains the ability to handle loading files from multiple sources
 */

class FileSystem {
public:
	FileSystem();

	std::vector<uint8_t> getFileAsBytes(const std::string& handle);
	std::string getFileAsString(const std::string& handle);
};

}
