#pragma once

#include <vector>
#include "sol.hpp"

namespace ybs {

class LuaDelegate {
public:
	void attach(sol::function f);
	bool detach(sol::function f);

	template <typename... A>
	void call(A&&... args) {
		//call all attached functions
		for (auto& i : mFunctions) {
			i(args...);
		}
	}
protected:
	std::vector<sol::protected_function> mFunctions;
};

}
