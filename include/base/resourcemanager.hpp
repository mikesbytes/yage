#pragma once

#include <unordered_map>
#include <memory>

namespace ybs {

/// Shared resource manager
/** Used for managing resources such as shaders, images, textures, etc. Works with any type.
 *  All instances of a specific type's ResourceManager share the same resource map, and therefore
 *  the same resources
 */
template<typename T>
class ResourceManager {
public:
	/// add a shared resource
	void addResource(const std::string& handle, std::shared_ptr<T> resource) {
		auto it = mResources.find(handle);
		if (it == mResources.end()) {
			mResources.insert(std::make_pair(handle, resource));
		}
	}

	template<typename... Args>
	void newResource(const std::string& handle, Args... args) {
		auto ptr = std::make_shared<T>(args...);
		addResource(handle, ptr);
	}

	/// Removes a shared resource
	/** Returns the removed resource or nullptr if it doesn't exist */
	std::shared_ptr<T> removeResource(const std::string& handle) {
		auto it = mResources.find(handle);
		if (it != mResources.end()) {
			auto ret = it->second;
			mResources.erase(it);
			return ret;
		}
		return nullptr;
	}

	/// Get a shared resource
	/** Returns the resource or nullptr if it doesn't exist */
	std::shared_ptr<T> getResource(const std::string& handle) {
		auto it = mResources.find(handle);
		if (it != mResources.end()) {
			return it->second;
		}
		return nullptr;
	}

protected:
	static inline auto mResources = std::unordered_map<std::string, std::shared_ptr<T>>();
};
}
