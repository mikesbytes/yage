#pragma once

#include <chrono>

namespace ybs {

class Clock {
public:
	Clock();
	double restart();

protected:
	std::chrono::steady_clock::time_point mLastStart;
};

}
