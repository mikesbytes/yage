#pragma once

#include <algorithm>

namespace ybs {

template<typename T>
T furthestFromZero(const T& lhs, const T& rhs) {
	auto lhsAbs = std::max(-lhs, lhs); // absolute value
	auto rhsAbs = std::max(-rhs, rhs);
	if (lhsAbs > rhsAbs) {
		return lhs;
	}
	return rhs;	                  
}

}
