#pragma once

#include <vector>

namespace ybs {

/// A row major 2D array implementation
template <typename T>
class Array2D {
public:
	Array2D(const int& x, const int& y) :
		mData(x * y),
		mX(x),
		mY(y)
	{}

	T get(const int& x, const int& y) {
		return mData[x + y * mX];
	}

	void set(const int&x, const int& y, const T& value) {
		mData[x + y * mX] = value;
	}

	/// Takes a function with inputs x, y, value
	template<class UnaryFunction>
	void iterate(UnaryFunction f) {
		for (int i = 0; i < mData.size(); ++i) {
			f(i % mX, i / mX, mData[i]);
		}
	}

	int width() { return mX; }
	int height() { return mY; }
protected:
	int mX, mY;
	std::vector<T> mData;
};

}
