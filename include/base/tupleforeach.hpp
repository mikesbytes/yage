#pragma once

#include <tuple>

namespace ybs {

template<class F, class...Ts, std::size_t...Is>
void forEachInTuple(const std::tuple<Ts...> & tuple, F func, std::index_sequence<Is...>){
    using expander = int[];
    (void)expander { 0, ((void)func(std::get<Is>(tuple)), 0)... };
}

template<class F, class...Ts>
void forEachInTuple(const std::tuple<Ts...> & tuple, F func){
    forEachInTuple(tuple, func, std::make_index_sequence<sizeof...(Ts)>());
}

}
