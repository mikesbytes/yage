#include "sol.hpp"

#include "entt.hpp"

namespace yecs {

/// Register a component and return it's sol usertype
/**
 * Template is <T, Constructor Args>
 */

template<typename T>
sol::usertype<T> registerComponent(::sol::state& lua, sol::table& tab, const std::string& name) {
    auto meta = lua["entt"]["registry"];
    sol::usertype<T> usertype;
    meta["assign" + name] = &entt::registry<>::assign<T>;
    meta["remove" + name] = &entt::registry<>::remove<T>;
    usertype = tab.new_usertype<T>("name");

    return usertype;
}

template<typename T, typename... Args>
sol::usertype<T> registerComponentArgs(::sol::state& lua, sol::table& tab, const std::string& name) {
	auto meta = lua["entt"]["registry"];
	sol::usertype<T> usertype;
	if constexpr(std::is_default_constructible_v<T>) {
		meta["assign" + name] = sol::overload(&entt::registry<>::assign<T, Args...>,
		                                      &entt::registry<>::assign<T>);
		usertype = tab.new_usertype<T>("name", sol::constructors<T(), T(Args...)>());
	} else {
		meta["assign" + name] = &entt::registry<>::assign<T, Args...>;
		usertype = tab.new_usertype<T>("name", sol::constructors<T(Args...)>());
	}
	meta["remove" + name] = &entt::registry<>::remove<T>;
	return usertype;
}
	
}
