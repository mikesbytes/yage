#pragma once

#include <vector>
#include <utility>
#include <algorithm>

namespace yecs {

class ComponentListBase {

};

template<typename T>
class ComponentList : public ComponentListBase {
public:
	void addComponent(const unsigned& id, T& comp) {
		// keeping the list ordered is important
		// check if we can put it at the end
		// the program will segfault if we don't check for empty first
		if (mComponents.empty() || mComponents.back().first <= id) {
			mComponents.push_back(std::make_pair(id, std::move(comp)));
		} else {
			// find first component with larger id than the one we're inserting
			auto it = std::find_if(mComponents.begin(), mComponents.end(),
			                       [&id](auto&& pair) { return pair.first >= id; });

			// insert the new component before it
			mComponents.insert(it, std::make_pair(id, std::move(comp)));
		}
	}

	T* getComponent(const unsigned& id) {
		auto it = std::lower_bound(mComponents.begin(),
		                           mComponents.end(),
		                           [](auto&& l, auto&& r) { return l.first < r.first; });
		if (it != mComponents.end() && it->first == id) {
			return it->second;
		}
		return nullptr;
	}

	const std::vector<std::pair<unsigned, T>>& getComponentList() const {
		return mComponents;
	}

	int size() { return mComponents.size(); }

	std::pair<unsigned, T>& operator[](size_t n) {
		return mComponents[n];
	}

protected:
	std::vector<std::pair<unsigned, T>> mComponents;
};

}
