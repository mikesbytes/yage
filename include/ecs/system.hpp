#pragma once

namespace yecs {

class ECS;

class System {
public:
	System();
	virtual void update(const double& delta) = 0;
};

}
