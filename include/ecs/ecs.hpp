#pragma once

#include <typeinfo>
#include <typeindex>
#include <unordered_map>
#include <memory>

#include "ecs/componentlist.hpp"
#include "ecs/system.hpp"

namespace yecs {

class ECS {
public:
	/// Returns reference to proper ComponentList and creates it if it doesn't exist
	template<typename T>
	ComponentList<T>& getComponentList() {
		// I am not using dynamic cast as the typeinfo key of the map guarantees
		// that the cast will not result in undefined behavior and dynamic cast
		// is expensive

		auto it = mComponentLists.find(std::type_index(typeid(T)));
		if (it != mComponentLists.end()) {
			return (ComponentList<T>&)*(it->second);
		}

		mComponentLists[std::type_index(typeid(T))] = std::make_unique<ComponentList<T>>();
		return (ComponentList<T>&)*(mComponentLists[std::type_index(typeid(T))]);
	}

	template<typename T>
	T& newSystem() {
		mSystems.push_back(std::make_unique<T>(*this));
		T& sys = (T&)*(mSystems.back());
		return sys;
	}

	void update(const double& delta);
protected:
	std::unordered_map<std::type_index, std::unique_ptr<ComponentListBase>> mComponentLists;
	std::vector<std::unique_ptr<System>> mSystems;
};
}
