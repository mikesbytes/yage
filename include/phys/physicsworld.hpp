#pragma once

#include <vector>
#include <functional>
#include <entt.hpp>

#include "phys/tilemapcollider.hpp"
#include "phys/collider.hpp"
#include "phys/aabbcollider.hpp"


namespace yph {

class PhysicsWorld {
public:
	PhysicsWorld();
	void update(const double& delta, entt::registry<>& registry);

	TileMapCollider& addCollider(const TileMapCollider& col);
	AABBCollider& addCollider(const AABBCollider& col);

protected:
	//std::vector<std::reference_wrapper<Collider>> mColliders;

	std::vector<TileMapCollider> mTileMapColliders;
	std::vector<AABBCollider> mAABBColliders;
};

}
