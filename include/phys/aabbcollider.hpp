#pragma once

#include "phys/collider.hpp"

namespace yph {

class AABBCollider : public Collider
{
public:
	AABBCollider(const glm::vec2& pos, const glm::vec2& size);
	AABBCollider(const float& x, const float& y, const float& w, const float& h);

	virtual AABB aabb() const;
protected:
	glm::vec2 mSize;
};
}
