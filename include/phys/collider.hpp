#pragma once

#include "phys/aabb.hpp"
#include "phys/positionable.hpp"
#include "base/luadelegate.hpp"

namespace yph {

class Collider : public Positionable {
public:
	Collider(const glm::vec2& pos);
	Collider(const float& x, const float& y);
	virtual AABB aabb() const = 0;


	/// called with a (Collision) argument when the collider is touched
	ybs::LuaDelegate& touched();
	
protected:
	ybs::LuaDelegate mTouched;
};

}
