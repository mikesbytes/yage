#pragma once

#include "ecs/system.hpp"
#include "ecs/ecs.hpp"
#include "ecs/componentlist.hpp"
#include "phys/position.hpp"
#include "phys/aabbcollider.hpp"

namespace yph {

class PhysicsSystem : public yecs::System
{
public:
	PhysicsSystem(yecs::ECS& ecs);
	virtual void update(const double& delta);

protected:
	yecs::ComponentList<Position>& mPositions;
	yecs::ComponentList<AABBCollider>& mAABBColliders;
};

}
