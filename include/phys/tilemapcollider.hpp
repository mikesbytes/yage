#pragma once

#include <memory>

#include "base/array2d.hpp"
#include "phys/collision.hpp"
#include "phys/collider.hpp"

namespace yph {

class AABB;

class TileMapCollider : public Collider {
public:
	TileMapCollider(std::shared_ptr<ybs::Array2D<unsigned>> tiles, const glm::vec2& pos, const float& tileSize);

	Collision collide(const AABB& box);
	virtual AABB aabb() const;
protected:
	float mTileSize;
	std::shared_ptr<ybs::Array2D<unsigned>> mTiles;
};

}
