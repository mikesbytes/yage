#pragma once

#include <glm/glm.hpp>

namespace yph {

class Positionable {
public:
	Positionable();
	Positionable(const glm::vec2& pos);
	Positionable(const float& x, const float& y);

	void setPos(const glm::vec2& pos);
	void setPos(const float& x, const float& y);
	glm::vec2 getPos() const;
protected:
	glm::vec2 mPos;
};

}
