#pragma once

#include <glm/glm.hpp>

namespace yph {

struct Position {
	glm::vec2 position;
};

}
