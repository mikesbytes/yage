#pragma once

#include <glm/glm.hpp>

namespace yph {
struct Collision {
public:
	Collision(const bool& collidingA, const glm::vec2& colVecA);
	explicit operator bool() const;

	bool colliding;
	glm::vec2 colVec;
};
}
