#pragma once

#include <glm/glm.hpp>

#include "phys/collision.hpp"

namespace yph {
struct AABB {
public:
	AABB();
	AABB(const glm::vec2& pos, const glm::vec2& size);
	AABB(const float& x, const float& y, const float& w, const float& h);

	bool contains(const glm::vec2& point);
	glm::vec2 closestPointOnBoundsToPoint(const glm::vec2& point);
	Collision collide(const AABB& box) const;

	glm::vec2 pos;
	glm::vec2 size;
};
}
