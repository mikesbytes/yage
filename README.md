# YAGE: Yet Another Game Engine #

Building

Requirements:

- cmake

- SDL2

- glm

- luajit

Instructions:

1. Clone the project

2. Checkout submodules

3. `cd yage`

4. `mkdir build && cd build`

5. `cmake .. && make`